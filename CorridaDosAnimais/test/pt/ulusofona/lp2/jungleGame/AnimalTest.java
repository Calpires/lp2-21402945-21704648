/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import java.util.List;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author a21704648
 */
public class AnimalTest {
    
    public AnimalTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    
   
    @Test
    public void testMoverAnimal() {
        System.out.println("moverAnimal");
        Animal instance = new Animal();
        
        instance.posicaoActual=0;
        instance.energiaActual=6;
        instance.meta=10;
        instance.terrenos= new String[]{"T", "T"};
        instance.setVelocidadeBase(3);
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       
        int esperado=10;
        int real=instance.posicaoActual;
        
        assertEquals(esperado, real);
    }

     @Test
    public void testMoverAnimalF() {
        System.out.println("moverAnimal");
        Animal instance = new Animal();
        
        instance.posicaoActual=0;
        instance.energiaActual=6;
        instance.meta=10;
        instance.terrenos= new String[]{"L", "T"};
        instance.setVelocidadeBase(3);
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       
        int esperado=9;
        int real=instance.posicaoActual;
        
        assertEquals(esperado, real);
    }
    
}
