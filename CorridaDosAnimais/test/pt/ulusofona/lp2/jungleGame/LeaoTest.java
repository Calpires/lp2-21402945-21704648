/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author a21704648
 */
public class LeaoTest {
    
    public LeaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of moverAnimal method, of class Leao.
     */
    @Test
    public void testMoverAnimal() {
       System.out.println("moverAnimal");
        Animal instance = new Leao();
        instance.turno=1;
        instance.posicaoActual=0;
        instance.energiaActual=6;
        instance.meta=10;
        instance.terrenos= new String[]{"R", "T"};
        instance.setVelocidadeBase(3);
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       
        int esperado=9;
        int real=instance.posicaoActual;
        
        assertEquals(esperado, real);
    }

     @Test
    public void testMoverAnimalF() {
        System.out.println("moverAnimal");
        Animal instance = new Leao();
        
        instance.posicaoActual=0;
        instance.turno=0;
        instance.energiaActual=6;
        instance.meta=10;
        instance.terrenos= new String[]{"R", "T"};
        instance.setVelocidadeBase(3);
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       
        int esperado=0;
        int real=instance.posicaoActual;
        
        assertEquals(esperado, real);
    }
    
}
