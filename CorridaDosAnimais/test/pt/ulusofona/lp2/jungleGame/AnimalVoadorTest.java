/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author a21704648
 */
public class AnimalVoadorTest {
    
    public AnimalVoadorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of moverAnimal method, of class AnimalVoador.
     */
    @Test
    public void testMoverAnimal() {
        System.out.println("moverAnimal");
        AnimalVoador instance = new AnimalVoador();
        
        instance.posicaoActual=2;
        instance.energiaActual=3;
        instance.meta=10;
        instance.setVelocidadeBase(3);
        
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       int esperado=7;
       int real=instance.posicaoActual;
       
        assertEquals(esperado, real);
        
    }
    
    @Test
    public void testMoverAnimalF() {
        System.out.println("moverAnimal");
        AnimalVoador instance = new AnimalVoador();
        
        instance.posicaoActual=2;
        instance.energiaActual=0;
        instance.meta=10;
        instance.setVelocidadeBase(3);
        
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       int esperado=2;
       int real=instance.posicaoActual;
       
        assertEquals(esperado, real);
        
    }
    
}
