/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author a21704648
 */
public class TartarugaTest {
    
    public TartarugaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of moverAnimal method, of class Tartaruga.
     */
    @Test
    public void testMoverAnimal() {
       System.out.println("moverAnimal");
        Tartaruga instance = new Tartaruga();
        
        instance.posicaoActual=0;
        instance.energiaActual=2;
        instance.meta=10;
        instance.terrenos= new String[]{"A", "A", "T", "A"};
        instance.setVelocidadeBase(2);
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       
        int esperado=5;
        int real=instance.posicaoActual;
        
    
       
        
        assertEquals(esperado, real);
    }

    @Test
    public void testMoverAnimalF() {
       System.out.println("moverAnimal");
        Tartaruga instance = new Tartaruga();
        
        instance.posicaoActual=0;
        instance.energiaActual=2;
        instance.meta=10;
        instance.terrenos= new String[]{"R", "A", "A", "A"};
        instance.setVelocidadeBase(2);
        instance.moverAnimal();
        // TODO review the generated test code and remove the default call to fail.
       
        int esperado=2;
        int real=instance.posicaoActual;
        
    
       
        
        assertEquals(esperado, real);
    }
    
}
