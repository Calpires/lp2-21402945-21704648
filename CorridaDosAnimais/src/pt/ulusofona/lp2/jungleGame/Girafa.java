/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

/**
 *
 * @author Calpires
 */
public class Girafa extends Animal{

    public Girafa( String nome, int idAnimal, int idEspecie, int velocidadeBase, int energiaInicial,int meta,int inteligencia, int numeroDeAnimais) {
        super(nome, meta, idEspecie, velocidadeBase, energiaInicial, meta, inteligencia, numeroDeAnimais);
        
        
    }

    Girafa() {
    }

 
    
    

    @Override
    public void moverAnimal() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(!animalCortouMeta()){
            ++turno;
            terrenoActual=terrenos[posicaoActual];
            calcularAtrito();
            if("R".equals(terrenoActual)){
                atrito=2;
            }
            calcularDistancia();
            if(energiaActual<1 || distancia<1){
                recuperaEnergia();
            }else{
              posicaoActual+=distancia;
              gastarEnergia();
           }    
        }
    }
}
