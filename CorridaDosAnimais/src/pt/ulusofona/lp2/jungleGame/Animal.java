/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author tio spalla
 */

  
   public  class Animal {
    //declaracao de variavies do animal
    
    //Strings   
    private String nome, especieDesignacao;
    protected String terrenoActual;
    //ints
    private int idDoAnimal, idEspecie, velocidadeBase, energiaInicial,
            classificacaoFinal, inteligencia, favoritismo, totalValorPremios, 
            pontos, numeroDeAnimais, classificacaoAlternativa;    
    protected int  posicaoActual, energiaActual, turno, meta, atrito, distancia;
    //listas  
    private List <String> premiosString= new ArrayList<>();
    private  List <Premio> premiosRecebidos= new ArrayList<>();
    //arrays   
    protected String[] terrenos;
    private   String especiesDesignacao [] = new String []{"Aguia", "Girafa", "Gorila", "Humano", 
    "Leão", "Lebre", "Mocho", "Tartaruga", "Tigre", "Zebra"};  
    private int especiesFavoritismo [] = new int []{3,2,3,2,3,2,2,1,3,2};
              
    //contrutores
    

    public Animal(String nome, int id, int idEspecie, int velocidadeBase, int 
             energiaInicial, int meta, int inteligencia, int numerodeAnimais) {
        
        this.nome = nome;
        this.idDoAnimal = id;
        this.idEspecie = idEspecie;
        this.velocidadeBase = velocidadeBase;
        this.energiaInicial = energiaInicial;
        this.posicaoActual=0;
        this.energiaActual=energiaInicial;
        this.meta=meta;
        this.inteligencia=inteligencia;
        this.turno=0;
        this.numeroDeAnimais=numerodeAnimais;
        this.favoritismo=especiesFavoritismo[idEspecie];
        this.especieDesignacao=especiesDesignacao[idEspecie];
    }

    public Animal() {
    }
    
    
    
     
     //******************************metodos get********************************
    public int getPosicao(){
        return posicaoActual;
    }
    
    public String getImagePNG(){
        return null;
    }

    public String getNome() {
        return nome;
    }
       
     public int getClassificacaoFinal() {
        return classificacaoFinal;
    }

    public List<String> getPremiosString() {
        return premiosString;
    }

    public String getEspecieDesignacao() {
        return especieDesignacao;
    }

    public int getTotalValorPremios() {
        return totalValorPremios;
    }

    public int getFavoritismo() {
        return favoritismo;
    }

    public int getTurno() {
        return turno;
    }

    public int getEnergiaInicial() {
        return energiaInicial;
    }

    public int getVelocidadeBase() {
        return velocidadeBase;
    }

    public int getInteligencia() {
        return inteligencia;
    }

    public List<Premio> getPremiosRecebidos() {
        return premiosRecebidos;
    }

    

    public int getEnergiaActual() {
        return energiaActual;
    }  
     
     public int getIdEspecie() {
        return idEspecie;
    }
     
     //****************************metodos set ********************************
       
    public void setClassificacaoFinal(int classificacaoFinal) {
        this.classificacaoFinal = classificacaoFinal;
    }

    public void setTerrenos(String[] terrenos) {
        this.terrenos = terrenos;
    }

    public void setInteligencia(int inteligencia) {
        this.inteligencia = inteligencia;
    }

    public void setVelocidadeBase(int velocidadeBase) {
        this.velocidadeBase = velocidadeBase;
    }
   
    public void setClassificacaoAlternativa(int classificacaoAlternativa) {
        this.classificacaoAlternativa = classificacaoAlternativa;
    }
     
   public void setEnergiaActual(int energiaActual) {
        this.energiaActual = energiaActual;
    }
     
     //******************outros metodos do animal*********************************
    public void moverAnimal(){        
        if(!animalCortouMeta()){            
            ++turno;
            terrenoActual=terrenos[posicaoActual];
            calcularAtrito();
            calcularDistancia();
            if(energiaActual<1 || distancia<1){
                recuperaEnergia();
            }else{
              posicaoActual+=distancia;
              gastarEnergia();
           }
        }
    }
     
    
    public String escreverClassificacao(String critetioDesempate) {
        String output;
        if(critetioDesempate.equals("Favoritismo")){
            output="#"+classificacaoFinal+", "+nome+", "+especieDesignacao+", t"+turno;
        }else{
            output="#"+classificacaoAlternativa+", "+nome+", "+especieDesignacao+", t"+turno;
        }
        return output;
    }  

    public boolean animalCortouMeta(){
        return posicaoActual==meta;
    }
    
    public void gastarEnergia(){
        double temp=Math.floor(distancia-inteligencia*1/2);
        energiaActual-=(int)Math.max(temp, 1);
    }
        
    public void recuperaEnergia(){        
        energiaActual+=Math.max(Math.min(inteligencia*1/2, energiaInicial), 1);        
    }
    
    public void calcularAtrito(){
        if( "T".equals(terrenoActual)){            
            atrito=0;
        }else{
            atrito=1;
        }  
    }
    
    public void calcularDistancia(){
        distancia= (int)(velocidadeBase*energiaActual*0.5+1-atrito);
    }
     
    public void calcularPremios(){
      int premiosNaoAcumulaveis=0;
      //verifica o numero de premios recebidos nao cumulaveis
      for(Premio premioAtual:premiosRecebidos){
             if(!premioAtual.eAcumulavel()){
              premiosNaoAcumulaveis++;
          }
        }
         
       if(premiosNaoAcumulaveis>0){
          premiosRecebidos=premiosRecebidos.stream()
                                           .filter(p->!p.eAcumulavel())//filtra os premios nao acumulaveis
                                           .sorted((p1, p2)->p2.getValor()-p1.getValor())//ordena por valor dos premios
                                           .limit(1)//seleciona o premio com maior valor
                                           .collect(Collectors.toList());//transforma a lista em uma lista so com o premio de maior valor
        }
       //percore a lista dos premios recebidos e soma o valor total
        for(Premio premioActual:premiosRecebidos){
            totalValorPremios+=premioActual.getValor();
        }
        //manda escrever a premiacao
        escreverPremios();
    }
  
    public void escreverPremios(){
           String outputPremio;
           premiosString.add(nome+" , "+totalValorPremios);
           for(Premio premioAtual:premiosRecebidos){
               outputPremio="     "+premioAtual.getNome()+"  ,  "+premioAtual.getValor();
               premiosString.add(outputPremio);
           }          
        }
       
    public int calcularPontos(){
        pontos= numeroDeAnimais-classificacaoFinal+1;        
        return pontos;        
    }

}   
