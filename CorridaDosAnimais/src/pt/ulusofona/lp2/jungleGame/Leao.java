/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

/**
 *
 * @author Calpires
 */
public class Leao extends Animal{
    
    public Leao( String nome, int idAnimal, int idEspecie, int velocidadeBase, int energiaInicial,int meta,int inteligencia, int numeroDeAnimais) {
        super(nome, meta, idEspecie, velocidadeBase, energiaInicial, meta, inteligencia, numeroDeAnimais);
        
        
    }

    Leao() {
    }

 
    
    

    @Override
    public void moverAnimal() {
        if(!animalCortouMeta()){            
            ++turno;
            terrenoActual=terrenos[posicaoActual];
            calcularAtrito();          
            calcularDistancia();
            if(energiaActual<1 || turno <2 || distancia <1){
                recuperaEnergia();
            }else{
                if(posicaoActual+distancia>meta){
                    posicaoActual=meta;
                }else{
                posicaoActual+=distancia;
                gastarEnergia();
                }
            }
        }
    }
}    

   
    

