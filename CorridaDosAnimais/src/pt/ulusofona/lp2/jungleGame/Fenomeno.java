/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

/**
 *
 * @author Calpires
 */
public class Fenomeno {
    private  int id;
    private   int turno;
    private int intiligencaOriginal;
    private int velocidadeBaseOriginal;
  

    public Fenomeno(int id, int turno) {
        this.id = id;
        this.turno = turno;
    }

    public int getTurno() {
        return turno;
    }

    public int getId() {
        return id;
    }

    public void aplicaFenomeno(Animal animal){
        velocidadeBaseOriginal=animal.getVelocidadeBase();
        intiligencaOriginal=animal.getInteligencia();
          
        switch (id){
            //perda de oxigenio
            case 1:
                if(animal.getInteligencia()>1){                
                animal.setInteligencia(intiligencaOriginal-1);
                }
                break;
                
            //dopping
            case 2:                
                animal.setEnergiaActual(animal.getEnergiaActual()*2);
                break;
            
            //vento a favor    
            case 3:               
                animal.setVelocidadeBase(velocidadeBaseOriginal++);
                break;
            
            //vento contra
            case 4:
                if(animal.getVelocidadeBase()>1){
                animal.setVelocidadeBase(velocidadeBaseOriginal--);
                }
                break;
            
            //solidariedade
            case 5:
                if(animal.getVelocidadeBase()>4){
                    animal.setVelocidadeBase(velocidadeBaseOriginal-2);
                }
        }
    }
     
    public void  desfazerFenomeno(Animal animal){
         animal.setInteligencia(intiligencaOriginal);
         animal.setVelocidadeBase(velocidadeBaseOriginal);
    }
}
