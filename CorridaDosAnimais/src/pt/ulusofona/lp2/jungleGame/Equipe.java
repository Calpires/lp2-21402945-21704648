/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import java.awt.PageAttributes;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Calpires
 */
public class Equipe {
    private List<Animal> animais;
    private int pontos;
    private String especie;
    private int favoritismo;
    
    public Equipe(String especie, int favoritismo) {
        this.especie=especie;
        this.favoritismo=favoritismo;
        animais=new ArrayList<>();
    }
    
    public void addAnimalEquipe(Animal animal){
        animais.add(animal);
    }
    
    public void somarPontos(){
        animais.stream()
                .sorted((a1, a2)->a1.getClassificacaoFinal()-a2.getClassificacaoFinal())
                .limit(2)
                .forEach(a->pontos+=a.calcularPontos());        
    }

    public int getPontos() {
        return pontos;
    }

    public int getFavoritismo() {
        return favoritismo;
    }

    public Animal getAnimal(int posicao) {
        return animais.get(posicao);
    }

    public String getEspecie() {
        return especie;
    }

    
}
