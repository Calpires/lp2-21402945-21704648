/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import java.io.IOException;

/**
 *
 * @author Calpires
 */
public class InvalidSimulatorInputException extends IOException{
    
  private   int linha;

    public InvalidSimulatorInputException(int linha) {
        this.linha = linha;
    }
    
    
    
    
    public int getInputLine(){
        return linha;
    }
    
}
