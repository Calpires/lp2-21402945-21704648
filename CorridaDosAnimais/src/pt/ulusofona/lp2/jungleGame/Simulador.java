/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Jeje
 */
public class Simulador{
    
    //*****************************variavies***********************************
    private int numeroAnimais, numeroPremios, meta, numeroFenomenos;
    
    private List<Animal> animaisList = new ArrayList<>();
    private List<Equipe> listaEquipes=new ArrayList<>();
    private List<Fenomeno>fenomenos=new ArrayList<>();
    private List<String> tabelaPremios=new ArrayList<>();
    private List<String> classificacaoGeralFavoritismo= new ArrayList<>();
    private List<String> classificacaoGeralEnergiaInicial= new ArrayList<>();
    private List<String> classificacaoEquipas= new ArrayList<>();
    private List<Premio> premios= new ArrayList<>();
    
    private String dadosPista[];
    
    
    
   
    //****************metodo de leiturado ficheiro**********************
    //----------------------------------------------------------------------
    public void iniciaJogo (File ficheiroInicial, int comprimentoPista)throws IOException{
        this.meta=comprimentoPista-1;
        try {
            Scanner leitorFicheiro=new Scanner(ficheiroInicial);
            String linha=leitorFicheiro.nextLine();
            String dadosMN[]=linha.split(":");
            
            numeroAnimais=Integer.parseInt(dadosMN[0]);
            numeroPremios=Integer.parseInt(dadosMN[1]);
            numeroFenomenos=Integer.parseInt(dadosMN[2]);

            int linhaAnimais=0; 
            String dadosAnimais[];
            
            // *****ler adados dos animais e escrever na memoria****************
           while(linhaAnimais<numeroAnimais) {
                
                linha=leitorFicheiro.nextLine();                    
                dadosAnimais=linha.split(":");
                if(dadosAnimais.length!=6){
                    throw new InvalidSimulatorInputException(linhaAnimais+2);
                }
                    
                String nomeDoAnimal=dadosAnimais[0];
                int idAnimal=Integer.parseInt(dadosAnimais[1]);
                int idEspecie=Integer.parseInt(dadosAnimais[2]);
                int velocidadeBase=Integer.parseInt(dadosAnimais[3]);
                int energiaInicial=Integer.parseInt(dadosAnimais[4]);
                int inteligencia=Integer.parseInt(dadosAnimais[5]);
                    
               
                FactoryAnimal fabrica=new FactoryAnimal();
                Animal animal=fabrica.fabricarAnimal(nomeDoAnimal, idAnimal, 
                        idEspecie, velocidadeBase, energiaInicial, meta,
                        inteligencia, numeroAnimais);
                    

                
                animaisList.add(animal);
                linhaAnimais++;

            }
           
           //****************ler dados dos premios e escrever na memnoria********
            int linhaPremios=0;
            String dadosPremios[];
            while(linhaPremios<numeroPremios) {
                
                    linha = leitorFicheiro.nextLine();
                    dadosPremios= linha.split(":");
                    if(dadosPremios.length!=4){
                        throw new InvalidSimulatorInputException(numeroAnimais+linhaPremios+2);
                    }
                    // converter as Strings lidas para os tipos esperados
                    String nomeDoPremio = dadosPremios[0];
                    int valorDoPremio = Integer.parseInt(dadosPremios[1]);
                    int posicaoQueRecebe = Integer.parseInt(dadosPremios[2]);
                    boolean acumulavel= true;
                    
                    if(dadosPremios[3].equals("N")){
                        acumulavel=false;
                    }
                    
                    Premio premio = new Premio(nomeDoPremio, valorDoPremio,
                            posicaoQueRecebe, acumulavel);                  
                    premios.add(premio);

                    linhaPremios++;
            }
            
            //ler fenomenos
            int linhasFenomenos=0;
            String dadosFenomenos[];
            while(linhasFenomenos<numeroFenomenos){
                
                linha=leitorFicheiro.nextLine();                
                dadosFenomenos=linha.split(":");
                
                if(dadosFenomenos.length!=2){
                    throw new InvalidSimulatorInputException
                    (numeroAnimais+numeroPremios+linhasFenomenos+2);
                }
                
                Fenomeno fenomeno=new Fenomeno(Integer.parseInt(dadosFenomenos[0]),
                        Integer.parseInt(dadosFenomenos[1]));
                fenomenos.add(fenomeno);  
                           
                linhasFenomenos++;
            }
            
            //ler os dados da pista******************************************
            linha=leitorFicheiro.nextLine();
            dadosPista=linha.split("");
            if(dadosPista.length<2){
                throw new InvalidSimulatorInputException
                (numeroAnimais+numeroPremios+numeroFenomenos+2);
            }
              
            for(Animal animal:animaisList){
                animal.setTerrenos(dadosPista);
            }
              
            
            leitorFicheiro.close();
        }
        catch(FileNotFoundException exception) {
            System.err.println("Ficheiro nao encontrado");
        }       
    }
    
    
    
    //*************metodo que processa cada turno**************************
    public boolean processaTurno(){
        
        int i=0;
        while(i<animaisList.size()){
            Animal animalActual=animaisList.get(i);
                      
            fenomenos.stream()
                     .filter(f->f.getTurno()==animalActual.getTurno())
                     .forEach(f -> f.aplicaFenomeno(animalActual));
                                  
            animalActual.moverAnimal();
                
            fenomenos.stream()
                     .filter(f->f.getTurno()==animalActual.getTurno()-1)
                     .forEach(f -> f.desfazerFenomeno(animalActual));   
            
            i++;
            
        }
           
        return todosNaMeta() != true;      
    }
    
    //metodos para escrever resultados no ficheuri de texto
    public void escreverResultados(String filename) throws IOException{
        File ficheiro=new File(filename);
        if(!ficheiro.exists()){
            ficheiro.createNewFile();
        }
        FileWriter fw=new FileWriter( ficheiro );
        BufferedWriter bw=new BufferedWriter( fw );
        
        atribuiClassificacao("Favoritismo");
        for(String texto:classificacaoGeralFavoritismo){
            bw.write(texto);
            bw.newLine();
        }
        bw.newLine();
        bw.newLine();
        
        atribuiClassificacao("Energia inicial");
        for(String texto:getClassificacaoGeral("Energia Inicial")){
            bw.write(texto);
            bw.newLine();
        }
        bw.newLine();
        bw.newLine();
        
        preencherTabelaPremios();
        for(String texto:tabelaPremios){
            bw.write(texto);
            bw.newLine();
        }
        bw.newLine();
        bw.newLine();
        
        atribuiClassificacaoEquipas();
        for(String texto:classificacaoEquipas){
            bw.write(texto);
            bw.newLine();
        }
        
        bw.close();
        fw.close();
        
    }
    
    public String getBackgroundImagePNG(int idPosicao){
        
       if ( dadosPista==null ||"T".equals(dadosPista[idPosicao]) ){
         return "box-land.png";
       }
       
       switch (dadosPista[idPosicao]){
            case "A":
                return "box-water-2.png";           
            case "L":
                return "box-mud.png";           
            case "R":
                return "box-rocks-2.png";            
            default: return null;
        }
       
    }
     
    
    public List<String> getClassificacaoEquipas(){               
        return  classificacaoEquipas;
    }
    
    public List<String> getTabelaPremios(){
        return tabelaPremios;
    }
    
    public List<Animal> getAnimais(){
        return animaisList;
    }
    
    //metodo que faz a entrega de premios***************************
    void entregaPremios(){
       for(Animal animal:animaisList){
           premios.stream().filter(p->p.getPosicaoQueRecebe()==animal.getClassificacaoFinal())
                           .forEach(p->animal.getPremiosRecebidos().add(p));
           
           animal.calcularPremios();
       }       
    }
    
       //retorna a classificacao geral de acordo com o criterio de desenmpate    
    public List<String> getClassificacaoGeral(String criterioDesempate){
        if(criterioDesempate.equals("Favoritismo")){
            return classificacaoGeralFavoritismo;  
        }         
        return classificacaoGeralEnergiaInicial;     
    }

    
    
    //verifica se todos os animais ja chegaram a meta
    public boolean todosNaMeta(){
        return animaisList.stream()
                          .noneMatch((animal) -> (!animal.animalCortouMeta()));
    } 
    
    public void criarEquipas(){        
        //ordena os animais pela especie
        Collections.sort(animaisList,(Animal a, Animal b)->{
            return a.getIdEspecie()-b.getIdEspecie();
        });  
        
        //cria uma equipa e adiciona o primeiro da lista ordenada
        Equipe equipa=new Equipe(animaisList.get(0).getEspecieDesignacao(),
                animaisList.get(0).getFavoritismo());        
        equipa.addAnimalEquipe(animaisList.get(0));
        
        //adiciona a equipa na lista das equipas
        listaEquipes.add(equipa);
        
        //percore a lista ordenada de animais e verifica se 2 animais seguidos sao da mesma especie 
        for(int i=1;i<animaisList.size();i++){ 
            
            //se for da mesma especie adiciona a equipa
            if(animaisList.get(i).getIdEspecie()==animaisList.get(i-1).getIdEspecie()){
                equipa.addAnimalEquipe(animaisList.get(i));
            }else{
                
                //se for de outra especie cria outra equipa e adiciona o animal
                equipa=new Equipe(animaisList.get(i).getEspecieDesignacao(),
                        animaisList.get(i).getFavoritismo());               
                listaEquipes.add(equipa);
                equipa.addAnimalEquipe(animaisList.get(i));
            }
        }    
    }
    
//metodo que preenche a tabela premios
    public void preencherTabelaPremios() {
        String outputPremiacao="Tabela de Premios";
        tabelaPremios.add(outputPremiacao);
        //ordena os animais por valor dos premios,        
        Collections.sort(animaisList, (Animal a1, Animal a2) -> {
            if(a1.getTotalValorPremios()==a2.getTotalValorPremios()){
                return a2.getEspecieDesignacao().compareTo(a1.getEspecieDesignacao());
            }else{
                return a2.getTotalValorPremios()-a1.getTotalValorPremios();
            }
        });
        
        //adiciona a string da premiacao de cada animal na tabela premios
        for(Animal animalActual:animaisList){
            for(String stringAtual:animalActual.getPremiosString())
            tabelaPremios.add(stringAtual);  
        }
    }
    
    //metodo que atribui classificacao as equipas
    void atribuiClassificacaoEquipas(){
        String mensagemInicial="Classificacao por Equipas";
        classificacaoEquipas.add(mensagemInicial);
        
        //cria as equipas e soma os pontos de cada equipa
        criarEquipas();
        listaEquipes.forEach(Equipe::somarPontos);
        
        //ordena as equipas por numero de pontos
        Collections.sort(listaEquipes, (Equipe e1, Equipe e2)->{
          if(e1.getPontos()==e2.getPontos()){
              if(e1.getFavoritismo()==e2.getFavoritismo()){
                  return e1.getEspecie().compareTo(e2.getEspecie());
              }
              return e1.getFavoritismo()-e2.getFavoritismo();
          }
          return e2.getPontos()-e1.getPontos();
        });
        
        //escreve a classificacao das equipas
        listaEquipes.stream().map((equipaAtual) -> equipaAtual.getEspecie()+", "
                +equipaAtual.getPontos()+"Pts").forEach((output) -> {
            classificacaoEquipas.add(output);
        });
    }
     
    //metodo que atribui classificacao, e usa criterios de desempate diferentes
    public void atribuiClassificacao(String criteriodesempate){
        if(criteriodesempate.equals("Favoritismo")){
            
        //ordena a lista e desempata com favoritismo   
        Collections.sort(animaisList, (Animal a1, Animal a2)->{
            if(a1.getTurno()==a2.getTurno()){
                if(a1.getFavoritismo()==a2.getFavoritismo()){
                  return  a1.getEspecieDesignacao().compareTo(a2.getEspecieDesignacao());
                }
                return a1.getFavoritismo()-a2.getFavoritismo();
                }
            return a1.getTurno()-a2.getTurno();
        });
        
        //manda atribuir a classificacao final de cada animal
        int i=1;
        for(Animal animal:animaisList){
            animal.setClassificacaoFinal(i++);
        }
        
        //manda escrever na tabela de classificacao final os dados de cada animal
        animaisList.stream().forEach((Animal animal) -> {              
            classificacaoGeralFavoritismo.add(animal.escreverClassificacao(criteriodesempate));
        });
        
        classificacaoGeralFavoritismo.add("Desempate: favoritismo");
        //manda entregar os premios, desempatando com favoritismo
        entregaPremios();
        }else{
            //ordena a lista de animai por turno de chegada e desempata por energia inicial
            Collections.sort(animaisList, (Animal a1, Animal a2)->{
            if(a1.getTurno()-a2.getTurno()==0){
                if(a1.getEnergiaInicial()==a2.getEnergiaInicial()){
                    return a1.getEspecieDesignacao().compareTo(a2.getEspecieDesignacao());
                }
                return a1.getEnergiaInicial()-a2.getEnergiaInicial();
            }
            return a1.getTurno()-a2.getTurno();
         
            });
            int i=1;
            for(Animal animal:animaisList){
                 animal.setClassificacaoAlternativa(i++);
            }
            //manda escrever a classificacao dos animais 
            animaisList.stream().forEach((animal) -> {
                classificacaoGeralEnergiaInicial.add(animal.escreverClassificacao(criteriodesempate));                 
            });
        classificacaoGeralEnergiaInicial.add("Desempate: Energia Inicial");             
        }        
    }
     
    
}
   
    
