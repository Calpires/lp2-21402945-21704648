/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

/**
 *
 * @author Jeje
 */
public class FactoryAnimal {
    public Animal fabricarAnimal(String nomeDoAnimal, int idAnimal, int idEspecie,
        int velocidadeBase, int energiaInicial, int meta, int inteligencia,int numeroAnimais){
        switch(idEspecie){
            case 0: 
                return  new AnimalVoador(nomeDoAnimal, idAnimal, idEspecie, 
                velocidadeBase, energiaInicial, meta, inteligencia, numeroAnimais);
                             
                            
                case 1:
                    return new Girafa(nomeDoAnimal, idAnimal, idEspecie, 
                    velocidadeBase, energiaInicial, meta, inteligencia, numeroAnimais);
                            
                            
                case 2:
                    return   new Gorila(nomeDoAnimal, idAnimal, idEspecie, 
                    velocidadeBase, energiaInicial, meta, inteligencia, numeroAnimais);
                            
                case 4:
                    return new Leao(nomeDoAnimal, idAnimal, idEspecie, 
                    velocidadeBase, energiaInicial, meta, inteligencia, numeroAnimais);
                            
                case 7:
                    return   new Tartaruga(nomeDoAnimal, idAnimal, idEspecie, 
                    velocidadeBase, energiaInicial, meta, inteligencia, numeroAnimais);
                            
                case 6:
                    return new AnimalVoador(nomeDoAnimal, idAnimal, idEspecie, 
                    velocidadeBase, energiaInicial, meta, inteligencia, numeroAnimais);
                           
                default: 
                    return  new Animal(nomeDoAnimal, idAnimal, 
                    idEspecie, velocidadeBase, energiaInicial, meta, inteligencia, numeroAnimais);
        }
    }
}
