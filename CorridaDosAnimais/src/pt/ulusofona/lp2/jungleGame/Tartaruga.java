/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

/**
 *
 * @author Calpires
 */
public class Tartaruga extends Animal{
    
  private  int bonusAgua;
    
  public Tartaruga( String nome, int idAnimal, int idEspecie, int velocidadeBase, int energiaInicial,int meta,int inteligencia, int numeroDeAnimais) {
        super(nome, meta, idEspecie, velocidadeBase, energiaInicial, meta, inteligencia, numeroDeAnimais);
        
        
    }

    public Tartaruga() {
    }

  
    
    

    @Override
    public void moverAnimal() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(!animalCortouMeta()){
            
            calcularAtrito();
            terrenoActual=terrenos[posicaoActual];
            if("A".equals(terrenoActual)){
                atrito=0;
            }
           calcularDistancia();
           if(energiaActual<1 || distancia<1){
                recuperaEnergia();
            }else{
               calculaBonus();
               if(posicaoActual+distancia+bonusAgua>meta){
                    posicaoActual=meta;
                }else{
                    posicaoActual+=distancia+bonusAgua;
                    gastarEnergia();
                }
            }
    }
}
private int calculaBonus(){
    bonusAgua=0;
    int i=posicaoActual;
    while(terrenos[i].equals("A") && i<i+distancia){
        bonusAgua++;
        i++;
        }
       return bonusAgua;
    } 
}

   
         
      
    
    
    

