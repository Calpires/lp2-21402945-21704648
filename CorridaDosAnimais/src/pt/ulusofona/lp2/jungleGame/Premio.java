/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pt.ulusofona.lp2.jungleGame;

/**
 *
 * @author Jeje
 */
public class Premio {
    
    //*********************variaveis**************************************
   private String nome;
   private int valor, posicaoQueRecebe;
   private boolean acumulavel;
    
    
    
    //*****************construtor*******************************************
    public Premio(String nome, int valor, int posicaoQueRecebe, boolean acumulavel) {
        this.nome = nome;
        this.valor = valor;
        this.posicaoQueRecebe = posicaoQueRecebe;
        this.acumulavel=acumulavel;
    }

    
//*************************************metodos get*************************
    public String getNome() {
        return nome;
    }

    public int getValor() {
        return valor;
    }
    
    int getPosicaoQueRecebe(){
        return posicaoQueRecebe;
    }
    
    boolean eAcumulavel(){
       return acumulavel==true;
    }

    
}

